﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WatchForFall : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject player = GameObject.Find ("FPSController");

        if (player.transform.position.y < -1) {
            TrackLevel.current_level = 0;
            SceneManager.LoadScene("GameOver");
        }
    }
}
