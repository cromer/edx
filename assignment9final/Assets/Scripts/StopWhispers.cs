﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopWhispers : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject whispers = GameObject.Find("WhisperSource");
        Destroy(whispers);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
