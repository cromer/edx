﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrackLevel : MonoBehaviour
{
    public static int current_level = 0;
    private Text m_TextComponent;

    // Start is called before the first frame update
    void Start()
    {
        GameObject level = GameObject.Find("Level");
        m_TextComponent = level.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        m_TextComponent.text = "Level: " + current_level;
    }
}
