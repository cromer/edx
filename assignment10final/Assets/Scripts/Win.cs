﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Win : MonoBehaviour
{
    private GameObject gameObject;
    private Text text;
    private Color color;

    // Start is called before the first frame update
    void Start()
    {
        gameObject = GameObject.Find("WinText");
        text = gameObject.GetComponent<Text>();
        color = text.color;

        // Make the text invisible
        color.a = 0.0f;
        text.color = color;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other) {
        // Show the text when player collides with finish line
        color.a = 1.0f;
        text.color = color;
	}
}
