﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemSpawner : MonoBehaviour
{
    public GameObject[] prefabs;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnGem());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SpawnGem() {
		while (true) {
			GameObject gem = Instantiate(prefabs[Random.Range(0, prefabs.Length)], new Vector3(26, Random.Range(-10, 10), 10), Quaternion.identity);
            gem.GetComponent<Renderer>().material.color = Color.red;
            
			yield return new WaitForSeconds(Random.Range(7, 15));
		}
	}
}
